# Stage 1: Build an Angular Docker Image
FROM node:16-alpine as build

ARG CI_COMMIT_SHA
ARG CI_COMMIT_MESSAGE

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY . /app

ARG configuration=production

RUN echo "{\"date\":\"`date`\"}" > /app/src/info.json

RUN npm run build -- --outputPath=./dist/out --configuration $configuration

RUN mkdir ./dist/out/.well-known && wget -O ./dist/out/.well-known/assetlinks.json "https://gitlab.com/p-fruck/dartstrainer-android/-/raw/main/assetlinks.json"

# Stage 2, use the compiled app, ready for production with Nginx
FROM nginx:alpine

COPY --from=build /app/dist/out/ /usr/share/nginx/html
COPY /nginx-custom.conf /etc/nginx/conf.d/default.conf
