# DartsTrainer

DartsTrainer is an open source PWA that assists you playing darts. It was developed by two German hobby dart players and is free to use and privacy friendly, since all of your data (names, game statistics) are stored locally on your device only.

You can access the application on any device that supports a web browser on [app.dartstrainer.eu](https://app.dartstrainer.eu) and, since it is a PWA, install it to your device, so it can also be used offline.

We also provide a wrapper for Ubuntu Touch, which can be downloaded from the OpenStore, as well as an Android wrapper available on Google Play.

<br>
<a href="https://open-store.io/app/eu.dartstrainer.app.clickable">
  <img width="200" height="60" alt="Download from the OpenStore" src="https://open-store.io/badges/en_US.svg">
</a>

<a href='https://play.google.com/store/apps/details?id=eu.dartstrainer.app.twa'>
    <img width="200" height="80" alt='Get it on Google Play'
        src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/>
</a>

<a href='https://f-droid.org/packages/eu.dartstrainer.app.twa/'>
    <img width="200" height="80" alt='Get it on F-Droid'
        src='https://fdroid.gitlab.io/artwork/badge/get-it-on.png'/>
</a>

## Selfhosting

You can easily host the application preview yourself using docker or any kind of webserver. The docker installation is pretty straight forward, simply run

```sh
docker run -p 8080:80 registry.gitlab.com/p-fruck/dartstrainer:dev
```

To run the application on a "normal" webserver, the application has to be compiled using NodeJS first. After installing NodeJS on your system, simply execute

```sh
npm ci && npm run build
```

to compile the typescript code into normal javascript. Afterwards, the dist folder of the application can be copied to the given webserver.

## Feedback

We are glad to hear your feedback! If you have any feature requests or found a bug in the application, feel free to create an issue.
