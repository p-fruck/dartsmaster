import { Component } from '@angular/core';
import { IconService, ThemeService } from '@app/core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  title = 'DartsTrainer';

  theme = 'default';

  constructor(
    public readonly themeService: ThemeService,
    iconService: IconService,
  ) {
    iconService.registerIcons();
  }
}
