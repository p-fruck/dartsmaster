import { Component } from '@angular/core';
import info from '@app/../info.json';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass'],
})
export class AboutComponent {
  info = info;
}
