import { AboutComponent } from './about/about.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SettingsComponent } from './settings/settings.component';

export {
  AboutComponent,
  NavigationComponent,
  SettingsComponent,
};
