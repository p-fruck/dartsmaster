import { Component } from '@angular/core';
import { ThemeService } from '@app/core/services';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.sass'],
})
export class SettingsComponent {
  constructor(
    public themeService: ThemeService,
  ) {}

  updateTheme(event: Event) {
    this.themeService.theme = (event.target as HTMLSelectElement).value;
  }
}
