import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { routes } from '@env/config';
import { GameService } from '.';

@Injectable()
export class GameGuard implements CanActivate {
  constructor(
    private gameService: GameService,
    private router: Router,
  ) { }

  isValid(route: ActivatedRouteSnapshot, path: string, gameState: string): boolean {
    return route.toString().includes(path) && this.gameService.game?.state === gameState;
  }

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    return [
      this.isValid(route, routes.bullOff, 'bull-off'),
      this.isValid(route, routes.game, 'started'),
    ].some((valid) => valid);
  }
}
