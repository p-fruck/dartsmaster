import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserDatabase } from '@app/core/db';
import { Game, User } from '@app/core/models';

@Injectable({
  providedIn: 'root',
})
export class IndexedDbService {
  private userDb = new UserDatabase();

  /**
   * Indicates whether current browser supports indexedDb. Problems exist with firefox in private
   * mode, see https://bugzilla.mozilla.org/show_bug.cgi?id=781982 for more details.
   */
  supportsIndexedDb = typeof (window.indexedDB as any)?.databases === 'function';

  constructor(
    private snackBar: MatSnackBar,
  ) {
    if (!this.supportsIndexedDb) {
      this.snackBar.open('Your browser doesn\'t support indexed db!', 'Dismiss', { duration: 5000 });
    }
  }

  get users() {
    return this.userDb.users;
  }

  async hasUser() {
    const userCount = await this.userDb.users.count();
    return userCount > 0;
  }

  // ToDo: Less duplications
  async addGame(game: Game): Promise<number> {
    return this.userDb.games
      .add(game)
      .then((id) => id)
      .catch((err) => {
        console.error(err);
        return 0;
      });
  }

  async addUser(user: User): Promise<User|undefined> {
    return this.userDb.users
      .add(user)
      .then((id) => this.userDb.users.get(id))
      .then((savedUser) => savedUser)
      .catch((err) => {
        console.error(err);
        return undefined;
      });
  }

  async getUser(id: number): Promise<User|undefined> {
    return this.userDb.users
      .get({ id });
    // // disabled because of setup wizard
    // .catch((err) => {
    //   console.error(err);
    //   return undefined;
    // });
  }

  async getUsers(): Promise<User[]> {
    return this.userDb.users.toArray();
  }

  async getGamesCount(userId: number): Promise<number> {
    return this.userDb.games
      .filter((game) => game.players.some((player) => player?.id === userId))
      .count();
  }
}
