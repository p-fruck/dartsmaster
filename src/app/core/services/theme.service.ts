import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private currentTheme = 'default';

  public darkmode = false;

  private themeList = [
    { key: 'default', name: 'Default Theme' },
    { key: 'red-cyan', name: 'Red-Cyan Theme' },
  ];

  get themes() {
    return this.themeList.map((theme) => theme);
  }

  get theme() {
    return this.currentTheme;
  }

  set theme(themeName: string) {
    if (this.themeList.some((theme) => theme.key === themeName)) {
      this.currentTheme = themeName;
    }
  }
}
