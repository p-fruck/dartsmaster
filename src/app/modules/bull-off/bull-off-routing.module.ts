import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BullOffComponent } from './bull-off.component';

const routes: Routes = [{ path: '', component: BullOffComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BullOffRoutingModule { }
