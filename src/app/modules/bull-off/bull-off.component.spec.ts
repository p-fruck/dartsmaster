import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BullOffComponent } from './bull-off.component';

describe('BullOffComponent', () => {
  let component: BullOffComponent;
  let fixture: ComponentFixture<BullOffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BullOffComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BullOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
