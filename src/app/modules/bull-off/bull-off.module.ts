import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';

import { BullOffRoutingModule } from './bull-off-routing.module';
import { BullOffComponent } from './bull-off.component';

@NgModule({
  declarations: [
    BullOffComponent,
  ],
  imports: [
    CommonModule,
    BullOffRoutingModule,
    DragDropModule,
    MatButtonModule,
  ],
})
export class BullOffModule { }
