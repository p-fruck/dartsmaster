import { IPlayerWinsResult, PlayerWinsComponent } from './player-wins/player-wins.component';
import { SingleDartInputComponent } from './inputs/single-dart-input/single-dart-input.component';

export {
  IPlayerWinsResult,
  PlayerWinsComponent,
  SingleDartInputComponent,
};
