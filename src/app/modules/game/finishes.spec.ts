import allFinishes from './finishes';

fdescribe('finishes', () => {
  const tokens = ['25', 'BULL'];
  for (let i = 1; i <= 20; i++) {
    ['', 'D', 'T'].forEach((prefix) => tokens.push(`${prefix}${i}`));
  }

  for (let dartsUsed = 0; dartsUsed < 3; dartsUsed++) {
    const finishes = allFinishes[dartsUsed % 3];
    for (let finish = 0; finish < finishes.length; finish++) {
      const ways = finishes[finish];
      if (JSON.stringify(ways) === '["No finish"]') continue; // skip finish if no way is defined
      ways.forEach((way) => {
        const throws = way.trim().split(' ');

        it('should have fewer throws than darts left', () => {
          expect(throws.length)
            .withContext(`on ${dartsUsed || 3} Darts used`)
            .toBeLessThanOrEqual(3 - dartsUsed);
        });

        it('should only use valid tokens', () => {
          expect(throws.every((t) => tokens.includes(t)))
            .withContext(`Throws: ${throws}`)
            .toBe(true);
        });

        it('should end with double value (double out)', () => {
          const last = throws[throws.length - 1];
          expect(last.startsWith('D') || last === 'BULL')
            .withContext(`Token ${last}`)
            .toBe(true);
        });

        it('should match the given score', () => {
          const score = throws.reduce(((prev, curr) => {
            if (curr === 'BULL') return prev + 50;
            if (curr.startsWith('D')) return prev + 2 * parseInt(curr.slice(1), 10);
            if (curr.startsWith('T')) return prev + 3 * parseInt(curr.slice(1), 10);
            return prev + parseInt(curr, 10);
          }), 0);
          expect(finish)
            .withContext(`${dartsUsed || 3} Darts used`)
            .toEqual(score);
        });
      });
    }
  }
});
