import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Gamemode } from '@app/core/gamemodes';
import { GameService } from '@app/core/services';
import { Player } from '@app/core/models/player';
import { routes } from '@env/config';
import { IPlayerWinsResult, PlayerWinsComponent } from './components';
import finishes from './finishes';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass'],
})
export class GameComponent {
  game!: Gamemode;

  boogies = [169, 168, 166, 165, 163, 162, 159];

  constructor(
    private gameService: GameService,
    private router: Router,
    private dialog: MatDialog,
  ) {
    if (!gameService.game) {
      // exit if no valid game is given
      router.navigate(['/']);
      return;
    }
    this.game = gameService.game;
    this.game.playerFinished$.subscribe((game) => this.onPlayerFinished(game));
  }

  getAverage(player: Player): number {
    // calculate three dart average like the PDC
    return ((this.game.initialScore - player.score) / player.darts.length) * 3;
  }

  getFinish(player: Player): string[] {
    const { score } = player;
    const dartsUsed = player.darts.length % 3;

    if (this.boogies.includes(score)) {
      return (['BOOGIE']);
    }
    if (finishes[dartsUsed].length > score) {
      return finishes[dartsUsed][score];
    }
    return (['no way so far']);
  }

  private onPlayerFinished(player: Player): void {
    // ask if game should continue
    const dialogRef = this.dialog.open(PlayerWinsComponent, {
      width: '350px',
      data: { player },
    });

    dialogRef.afterClosed().subscribe((result: IPlayerWinsResult) => {
      switch (result) {
        case 'exit': // save game and exit
          this.gameService.saveCurrentGame();
          this.router.navigate(['/']);
          break;
        case 'restart': // reset game
          if (!this.gameService.game) {
            this.router.navigate(['/']);
            break;
          }
          this.gameService.game.reset();
          if (this.gameService.game.players.length > 1) {
            this.router.navigate(['/', routes.bullOff], { queryParams: { rotate: true } });
          }
          break;
        default: // continue with current game
          break;
      }
    });
  }
}
