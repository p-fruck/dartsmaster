import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-x01',
  templateUrl: './x01.component.html',
  styleUrls: ['./x01.component.sass'],
})
export class X01Component {
  form: FormGroup;

  constructor(
    formBuilder: FormBuilder,
  ) {
    this.form = formBuilder.group({
      doubleIn: false,
      doubleOut: true,
      masterOut: false,
      pointsLevel: 3,
      customPoints: [501, Validators.min(2)],
    });
  }

  toggleDoubleOut(masterOut: MatSlideToggleChange) {
    if (masterOut.checked) {
      this.form.controls.doubleOut.setValue(true);
      this.form.controls.doubleOut.disable();
    } else {
      this.form.controls.doubleOut.enable();
    }
  }

  /**
   * Display selected point value in relation to the slider level
   * @param value - The slider level
   * @returns A string representing the number of points
   */
  formatLabel(value: number) {
    return ['FREE', '101', '301', '501', '701'][value];
  }

  get gameOptions() {
    const { pointsLevel } = this.form.value;
    return {
      ...this.form.value,
      points: pointsLevel === 0 ? this.form.value.customPoints : -99 + pointsLevel * 200,
    };
  }
}
