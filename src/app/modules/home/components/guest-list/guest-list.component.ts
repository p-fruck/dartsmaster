import { Component } from '@angular/core';
import {
  Validators, FormGroup, FormArray, FormBuilder,
} from '@angular/forms';

@Component({
  selector: 'app-guest-list',
  templateUrl: './guest-list.component.html',
  styleUrls: ['./guest-list.component.sass'],
})
export class GuestListComponent {
  private guestInputObj = {
    name: ['', Validators.required],
    type: 'guest',
  };

  guestForm: FormGroup;

  guests: FormArray;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.guestForm = this.formBuilder.group({
      guests: this.formBuilder.array([]),
    });
    this.guests = this.guestForm.get('guests') as FormArray;
    this.addGuest();
  }

  addGuest(): void {
    this.guests.push(
      this.formBuilder.group(this.guestInputObj),
    );
  }

  removeGuest(index: number): void {
    this.guests.removeAt(index);
  }
}
