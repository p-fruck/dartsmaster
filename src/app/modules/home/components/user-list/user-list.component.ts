import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { User } from '@app/core/models';
import { UserService } from '@app/core/services';
import { AddUserComponent } from '..';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.sass'],
})
export class UserListComponent implements OnInit {
  userForm = new FormControl();

  userList: User[] = [];

  constructor(public userService: UserService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.fetchUsers();
  }

  async fetchUsers() {
    this.userList = await this.userService.getUsers();
  }

  async addUser() {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe((name) => {
      if (name) {
        this.userService.createUser(name);
        this.fetchUsers();
      }
    });
  }
}
