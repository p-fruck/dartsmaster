import {
  AfterViewInit, Component, EventEmitter, Output, ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { User } from '@app/core/models';
import { GameService, IndexedDbService } from '@app/core/services';
import { X01 } from '@app/core/gamemodes';
import { routes } from '@env/config';
import { X01Component, GuestListComponent, UserListComponent } from './components';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements AfterViewInit {
  @ViewChild(GuestListComponent) private guestListComponent?: GuestListComponent;

  @ViewChild(UserListComponent) private userListComponent?: UserListComponent;

  @Output() finished = new EventEmitter<User[]>();

  viewIsReady = false;

  @ViewChild(X01Component)
  private x01Component!: X01Component;

  // indicates whether player- or guest-selector should be displayed
  supportsIndexedDb: boolean;

  constructor(
    private router: Router,
    private gameService: GameService,
    indexedDbService: IndexedDbService,
  ) {
    this.supportsIndexedDb = indexedDbService.supportsIndexedDb;
  }

  ngAfterViewInit(): void {
    this.viewIsReady = true;
  }

  startGame() {
    this.gameService.startGame(
      new X01(this.players, this.x01Component.gameOptions),
    );
    if (this.players.length > 1) {
      this.router.navigate(['/', routes.bullOff]);
    } else {
      this.gameService.game?.bullOff(this.gameService.game.players);
      this.router.navigate(['/', routes.game]);
    }
  }

  get guestsOnly(): User[] {
    const raw: User[] = this.guestListComponent?.guests.value as [] || [];
    return raw.filter((guest) => guest.name);
  }

  get usersOnly(): User[] {
    return this.userListComponent?.userForm.value ?? [];
  }

  /**
   * Guests and users combined
   */
  get players(): User[] {
    return this.guestsOnly.concat(this.usersOnly);
  }
}
