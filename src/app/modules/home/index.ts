import { HomeComponent } from './home.component';
import { HomeModule } from './home.module';
import { HomeRoutingModule } from './home-routing.module';

export {
  HomeComponent,
  HomeModule,
  HomeRoutingModule,
};
