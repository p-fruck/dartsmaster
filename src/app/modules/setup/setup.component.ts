import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@app/core/services';
import { routes } from '@env/config';

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.sass'],
})
export class SetupComponent {
  playerForm: FormGroup;

  constructor(
    private router: Router,
    private userService: UserService,
    formBuild: FormBuilder,
  ) {
    this.playerForm = formBuild.group({
      name: ['', Validators.required],
    });
  }

  async submit(): Promise<void> {
    if (!this.playerForm.valid) return;
    await this.userService.createUser(this.playerForm.value.name);
    this.continueToHome();
  }

  continueToHome() {
    this.router.navigate(['/', routes.home]);
  }
}
