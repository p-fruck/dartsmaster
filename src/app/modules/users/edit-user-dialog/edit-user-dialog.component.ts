import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '@app/core/models/user';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.sass'],
})
export class EditUserDialogComponent {
  userForm = new FormGroup({
    name: new FormControl(this.user.name, Validators.required),
  });

  constructor(
    public dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private user: User,
  ) { }

  get originalName() {
    return this.user.name;
  }

  updateUser() {
    return {
      ...this.user,
      ...this.userForm.value,
    };
  }
}
