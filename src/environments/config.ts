import { environment } from './environment';

const {
  production,
  routes,
} = environment;

export {
  production,
  routes,
};
