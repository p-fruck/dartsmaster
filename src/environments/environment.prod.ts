export const environment = {
  production: true,
  routes: {
    about: 'about',
    bullOff: 'bull-off',
    game: 'game',
    home: 'home',
    settings: 'settings',
    setup: 'setup',
    users: 'users',
  },
};
